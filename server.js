// load the things we need
var express = require('express');
var socket = require('socket.io')
var app = express();

//  //set the view engine to ejs
// app.set('view engine', 'ejs');

// //use res.render to load up an ejs view file

// // index page 
// app.get('/', function(req, res) {
// 	res.render('pages/index');
// });

// // about page 
// app.get('/about', function(req, res) {
// 	res.render('pages/about');
// });

 var server = app.listen(3000);
// console.log('3000 is the magic port');
app.use(express.static('public'))

var io =socket(server)
io.on('connection',(socket)=>{
	console.log('socket connection',socket.id)
	
    // Handle chat event
    socket.on('chat', function(data){
        // console.log(data);
        io.sockets.emit('chat', data);
	});
	
	  // Handle typing event
	  socket.on('typing', function(data){
        socket.broadcast.emit('typing', data);
    });
})			